import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.zip.GZIPInputStream;

public class TextStream {

	private ArrayList<String> folders = new ArrayList<String>();
	private int nextFolder = -1;
	private ArrayList<String> files = new ArrayList<String>();
	private int nextFile = -1;
	DataInputStream data_input = null;
	

	public TextStream(String dataDirectory) {
		File dir = new File(dataDirectory);
		if (dir.isDirectory()) {
			File[] dir_files = dir.listFiles();
			Arrays.sort(dir_files);
			for (int i = 0; i < dir_files.length; ++i) {
				if (dir_files[i].isDirectory()) {
					folders.add(dir_files[i].getAbsolutePath());					
				} else if (dir_files[i].isFile()) {
					System.out.println(dir_files[i].getAbsolutePath());
					files.add(dir_files[i].getAbsolutePath());
				}
					
			}
			if (files.size() > 0)
				nextFile = 0;
			if (folders.size() > 0) {
				nextFolder = 0;
				while (nextFile < 0) {
					expandNextFolder();
					if (files.size() > 0)
						nextFile = 0;
				}
			}
		}
	}

	private void expandNextFolder() {		
		files.addAll(getFilePaths(folders.get(nextFolder)));
		incrementNextFolder();
	}

	private ArrayList<String> getFilePaths(String folder_path) {
		ArrayList<String> filePaths = new ArrayList<String>();
		File dir = new File(folder_path);
		File[] dir_files = dir.listFiles();
		Arrays.sort(dir_files);
		for (int i = 0; i < dir_files.length; ++i)
			filePaths.add(dir_files[i].getAbsolutePath());
		return filePaths;
	}

	private void incrementNextFolder() {
		nextFolder++;
		if (nextFolder >= folders.size())
			nextFolder = 0;
	}

	private void incrementNextFile() {
		if (nextFolder < 0 || nextFolder >= folders.size()) return;
		
		++nextFile;
		if (nextFile >= files.size()) {
			files.clear();
			nextFile = -1;				
				
			while (nextFile < 0 && nextFolder < folders.size()) {
				expandNextFolder();
				if (files.size() > 0)
					nextFile = 0;
			}
		}
	}

	public ArrayList<byte[]> readNextLinesBatch(int batchSize) {
		if (data_input == null)
			openNextFile();

		ArrayList<byte[]> lines = new ArrayList<byte[]>();

		try {
			for (int i = 0; i < batchSize; ++i) {
				if (data_input.available() > 0) {
					byte[] el = new byte[19];
					data_input.readFully(el);
					lines.add(el);
				} else {
					openNextFile();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return lines;

		/*
		 * if(stream == null) openNextFile();
		 * 
		 * ArrayList<String> lines = new ArrayList<String>(); for(int i = 0; i <
		 * batchSize; ++i) { String line = null; try { line = stream.readLine();
		 * } catch (IOException e) { e.printStackTrace(); } if(line != null)
		 * lines.add(line); else openNextFile(); } return lines;
		 */
	}

	private void openNextFile() {
		try {
			String file = files.get(nextFile);

			data_input = new DataInputStream(new FileInputStream(file));
			
			// skip file header
			for (int i = 0; i < 32; ++i) {
				data_input.readLine();
			}		

			incrementNextFile();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * try { FileInputStream fileStream = new
		 * FileInputStream(files.get(nextFile)); InputStream gzipStream = new
		 * GZIPInputStream(fileStream); Reader decoder = new
		 * InputStreamReader(gzipStream, "US-ASCII"); stream = new
		 * BufferedReader(decoder); incrementNextFile(); } catch
		 * (FileNotFoundException e) { e.printStackTrace(); } catch (IOException
		 * e) { e.printStackTrace(); }
		 */
	}
}
