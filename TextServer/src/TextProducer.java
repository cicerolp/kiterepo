import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

import edu.umn.cs.kite.preprocessing.TweetJSONPreprocessor;
import edu.umn.cs.kite.util.microblogs.Tweet;


public class TextProducer implements Runnable {

	private ArrayBlockingQueue<String> queue;
	private String dataDirectory;
	private int bufferSize;
	private TextStream stream;
	private Integer tweet_id = 0;
	private int insertion_rate;
	
	public TextProducer(ArrayBlockingQueue<String> queue,
			String dataDirectory, int bufferSize, int insertion_rate) {
		this.queue = queue;
		this.dataDirectory = dataDirectory;
		this.bufferSize = bufferSize;
		this.stream = new TextStream(dataDirectory);
		this.insertion_rate = insertion_rate;
	}

	@Override
	public void run() {
		while(true)
		{
			readData();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void readData() {
		while(queue.size() < bufferSize)
		{
			int batchSize = Math.min(10000, bufferSize-queue.size());
			ArrayList<byte[]> lines = stream.readNextLinesBatch(batchSize);
			ArrayList<String> preprocessed_lines = preprocessJSONTweets(lines);
			queue.addAll(preprocessed_lines);
		}
	}

	private ArrayList<String> preprocessJSONTweets(ArrayList<byte[]> lines) {
		ArrayList<String> preprocessed_lines = new ArrayList<String>();

		for(byte[] el : lines) {
			Float lat = ByteBuffer.wrap(el, 0, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
			Float lon = ByteBuffer.wrap(el, 4, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
			
			// "2:"+lat+":"+lng
			String id = tweet_id.toString();
			String timestamp = Integer.toString((tweet_id / insertion_rate) * 1000);
			String username = new String("username");
			String location = new String("2:" + lat.toString() + ":" + lon.toString());
			String text = new String("text");
			
			// "" + id + "," + timestamp + "," + username + "," + geolocation + "," + getText()
			String el_str = new String("" + id + "," + timestamp + "," + username + "," + location + "," + text);
			
			//System.out.println(el_str);
			
			preprocessed_lines.add(el_str);
			
			tweet_id++;
		}
		
		return preprocessed_lines;
	}
}
