package edu.umn.cs.kite.util;

public class PointLocation implements GeoLocation {

	private double lat,lng;
	private boolean valid = false;

	public PointLocation(double lat, double lng) {
		this.lat = lat;
		this.lng = lng;
	};
	public PointLocation(double [] coords) {
		if(coords == null || coords.length < 2)
			return;
		this.lat = coords[0];
		this.lng = coords[1];
		valid = true;
	}
	public boolean isValid() {
		return valid;
	};
	public String toString()  {
		if(isValid())
			return "2:"+lat+":"+lng;
		else
			return "null";
    }
}
