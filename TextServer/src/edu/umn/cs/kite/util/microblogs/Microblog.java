package edu.umn.cs.kite.util.microblogs;

/**
 * Created by amr_000 on 8/1/2016.
 */

import java.util.ArrayList;
import java.io.Serializable;

import edu.umn.cs.kite.util.GeoLocation;

public abstract class Microblog implements Serializable
{
    private long id;
    private long timestamp;
    private ArrayList<String> keywords;
    private GeoLocation geolocation;
    private String username;

    public void setId(long id) { this.id = id; }
    public void setTimestamp(long timestamp) { this.timestamp = timestamp;}
    public void setKeywords(ArrayList<String> keywords) { this.keywords = keywords; }
    public void setGeoLocation(GeoLocation loc) { this.geolocation = loc; }
    public void setUsername(String username) { this.username = username; }

    public long getId() { return id; }

    public ArrayList<String> getKeywords() { return this.keywords; }

    private String getText() {
        String textual_rep = "";
        if(keywords.size() > 0)
        {
            for(int i = 0; i < keywords.size(); ++i)
                textual_rep += keywords.get(i)+",";
        }
        return textual_rep.substring(0, textual_rep.length()-1);
    }
    public String toString()
    {
        return "" + id + "," + timestamp + "," + username + "," + geolocation + "," + getText();
    }
}
