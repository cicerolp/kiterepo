package edu.umn.cs.kite.util.microblogs;

/**
 * Created by amr_000 on 8/1/2016.
 */

import java.util.ArrayList;

import edu.umn.cs.kite.util.GeoLocation;

public class Tweet extends Microblog {
    public Tweet() {}
    public Tweet(long id, long timestamp, ArrayList<String> keywords,
                 GeoLocation loc, String username) {
        setId(id);
        setTimestamp(timestamp);
        setKeywords(keywords);
        setGeoLocation(loc);
        setUsername(username);
    }
}
