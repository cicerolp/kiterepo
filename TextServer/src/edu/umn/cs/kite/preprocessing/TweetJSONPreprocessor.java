package edu.umn.cs.kite.preprocessing;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.umn.cs.kite.util.GeoLocation;
import edu.umn.cs.kite.util.PointLocation;
import edu.umn.cs.kite.util.microblogs.Tweet;

/**
 * Created by amr_000 on 8/8/2016.
 */
public class TweetJSONPreprocessor implements Preprocessor<String,Tweet> {
    @Override
    public Tweet preprocess(String jsonTweet) {
        return JSON_to_Tweet(jsonTweet);
    }

    @Override
    public List<Tweet> preprocess(List<String> objects) {
        List<Tweet> tweets = new ArrayList<>(objects.size());
        for(String line : objects) {
            Tweet tweet = preprocess(line);
            if(tweet != null)
                tweets.add(tweet);
        }
        return tweets;
    }

    private static Tweet JSON_to_Tweet(String jsonLine)
    {
        try
        {
            JSONObject object = new JSONObject(jsonLine);
            //int id = object.getInt("id");
            long id = object.getLong("id");
            Date timestamp = new Date(object.getString("created_at"));
            String text = object.getString("text");

            JSONObject user = object.getJSONObject("user");
            String username = user.getString("screen_name");
            
            String [] words = text.split("\\s+");
            ArrayList<String> keywords = new ArrayList<String>();
            for(String word:words)
                keywords.add(word);

            PointLocation location = new PointLocation(getGeoCoordsJSONObj(object));
            if(!location.isValid()) location = null;
            return new Tweet(id, timestamp.getTime(), keywords, location, username);
        } catch(org.json.JSONException e) {
            return null;
        }
    }
    
    private static double [] getGeoCoordsJSONObj(JSONObject post) {
		//getting geo tag
		double [] coords = new double[2];
		try
		{
			JSONObject geo = post.getJSONObject("geo");
			JSONArray coordsJSON = (JSONArray) geo.get("coordinates");
			coords[0] = coordsJSON.getDouble(0);
			coords[1] = coordsJSON.getDouble(1);
		}
		catch(org.json.JSONException e)
		{
			try
			{
				JSONObject place = post.getJSONObject("place");
				JSONObject placeBoundingBox = place.getJSONObject("bounding_box");
				
				JSONArray tmpCoords = (JSONArray) placeBoundingBox.get("coordinates");
				
				
				double lng = 
						(tmpCoords.getJSONArray(0).getJSONArray(0).getDouble(0) + 
						 tmpCoords.getJSONArray(0).getJSONArray(1).getDouble(0) + 
						 tmpCoords.getJSONArray(0).getJSONArray(2).getDouble(0) + 
						 tmpCoords.getJSONArray(0).getJSONArray(3).getDouble(0))/4;
				double lat = 
						(tmpCoords.getJSONArray(0).getJSONArray(0).getDouble(1) + 
						 tmpCoords.getJSONArray(0).getJSONArray(1).getDouble(1) + 
						 tmpCoords.getJSONArray(0).getJSONArray(2).getDouble(1) + 
						 tmpCoords.getJSONArray(0).getJSONArray(3).getDouble(1))/4;
				coords[0] = lat;
				coords[1] = lng;
			}
			catch(org.json.JSONException e2)
			{
				coords = null;
				return coords;
			}
		}
		return coords;
	}
}
