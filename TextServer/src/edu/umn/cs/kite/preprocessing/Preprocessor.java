package edu.umn.cs.kite.preprocessing;

import java.util.List;

/**
 * Created by amr_000 on 8/8/2016.
 */
public interface Preprocessor<S,T> {
    public T preprocess(S object);
    public List<T> preprocess(List<S> objects);
}
