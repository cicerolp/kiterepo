import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.concurrent.ArrayBlockingQueue;


public class ClientHandler implements Runnable {

	private Socket clientSocket, comm_socket;
	private ArrayBlockingQueue<String> queue;
	private int linesRate;
	private int maxRate;
	private int id;
	private int target_bench;
	
	public ClientHandler(int clientId, Socket client, Socket comm_socket, 
			ArrayBlockingQueue<String> queue, int linesRate, int maxRate, int target_bench) {
		this.clientSocket = client;
		this.comm_socket = comm_socket;
		this.queue = queue;
		this.linesRate = linesRate;
		this.maxRate = maxRate;
		this.target_bench = target_bench;
		id = clientId;
	}
	@Override
	public void run() {
		handleClient();		
	}
	private void handleClient() {
		try {
			
			BufferedReader comm_in = new BufferedReader(new InputStreamReader(comm_socket.getInputStream()));
			PrintWriter comm_out = new PrintWriter(comm_socket.getOutputStream(), true);
			
		    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); 
		    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
	
		    String inputLine; 
	
		    Date lastConnection = new Date();
		    long connections = 0;
		    
		    boolean kite_initialized = false;
		    int accum = 0;
		    
		    while ((inputLine = in.readLine()) != null) {
				if (inputLine.toLowerCase().compareTo("data") == 0) {
					
					if (!kite_initialized) {
						System.out.println("Kite is " + comm_in.readLine());
						kite_initialized = true;
					}
					
					if (accum >= target_bench) {
						comm_out.println("finish");
						
						System.out.println("Signal: " + comm_in.readLine());
						
						comm_out.close();
						comm_in.close();
					    out.close();
						in.close();
						comm_socket.close();
						clientSocket.close();
						
						System.exit(0);
					} else {
						double duration_seconds = ((new Date().getTime() - lastConnection.getTime())*1.0/1000.0);
					    int batchSize = connections==0? linesRate: (int) duration_seconds*linesRate;
					    batchSize = Math.min(batchSize, maxRate);
					    batchSize = Math.max(batchSize, linesRate);
					    
					    lastConnection = new Date();
					    ++connections;
					    
					    //writing output to client
					    System.out.println("Sending "+batchSize+" lines... to client "+id);
					    out.println(""+batchSize);
					    
					    for(int i = 0; i < batchSize; ++i) {
					    	String line = queue.take();
					    	out.println(line);
					    }
					    
					    System.out.println("Data sent successfully to client "+id);
					    
					    comm_out.println(Integer.toString(batchSize));
					    accum += batchSize;	
					}				    
				} else if (inputLine.toLowerCase().compareTo("close") == 0) {
					comm_out.close();
					comm_in.close();
				    out.close();
					in.close();
					comm_socket.close();
					clientSocket.close();	
				}
	        }
		} catch(IOException e){
			System.err.println(e.getMessage());
			System.out.println("Client "+id+ " is disconnedted");
		} catch (InterruptedException e) {
			System.err.println(e.getMessage());
			System.out.println("Client "+id+ "is disconnedted");
		} catch(Exception e) {
			System.err.println(e.getMessage());
			System.out.println("Client "+id+ "is disconnedted");
		}
	}

}
