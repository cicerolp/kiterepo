import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;


public class TextConsumer implements Runnable {

	private ArrayBlockingQueue<String> queue;
	private int port;
	private int linesRate;
	private int maxRate;
	private int target_bench;
	private String TCP_or_UDP;
	private boolean close = false;

	public TextConsumer(ArrayBlockingQueue<String> queue, int port,
			int linesRate, int maxRate, String TCP_or_UDP, int target_bench) {
		this.queue = queue;
		this.port = port;
		this.linesRate = linesRate;
		this.maxRate = maxRate;
		this.TCP_or_UDP = TCP_or_UDP.toLowerCase();
		this.target_bench = target_bench;
	}

	@Override
	public void run() {
		if(TCP_or_UDP.compareTo("tcp")==0)
		{
			runTCPServer();
		}
	}

	private void runTCPServer() {
		ServerSocket serverSocket = null; 

	    try { 
	         serverSocket = new ServerSocket(port); 
	    } 
	    catch (IOException e) 
        { 
	    	System.err.println("Could not listen on port: "+port); 
        	System.exit(1); 
        } 

	  
	    int clients = 0;
	   
	    while(!close) {
		    System.out.println ("Waiting for connection.....");
	
		    try { 
		    	Socket clientSocket = serverSocket.accept(); 
			    System.out.println ("Connection successful");
			    ++clients;
			    
			    Socket comm_socket = new Socket(InetAddress.getByName("localhost"), 9090);
			    
			    Thread clientThread = new Thread(new ClientHandler(clients, clientSocket, comm_socket, queue, linesRate, maxRate, target_bench));
			    clientThread.start();
			    
			} catch (IOException e) { 
				close = true;
	        	System.err.println("Skipping I/O exception. " + e.getMessage()); 
	        }
	    }
	     
	    try {
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	    
	}
}
