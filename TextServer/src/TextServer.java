import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;

//This is a socket server that work with a producer/consumer model
//The server reads lines of text from file system and send them over the network
//The producer/consumer queue has a limited size (bufferSize) and send data with a certain rate (linesRate)
//Server could work on TCP or UDP
public class TextServer {
	public static void main(String[] args) {
		runTextServer(args);
	}

	private static void runTextServer(String[] args) {
		String inputFile = "TextServer.in";
		if(args.length > 0)
			inputFile = args[0];
		
		System.out.println("Starting TextServer...");

		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(new File(inputFile)));
			int port = Integer.parseInt(br.readLine());
			String dataDirectory = br.readLine();
			int bufferSize = Integer.parseInt(br.readLine());
			int linesRate = Integer.parseInt(br.readLine());
			int maxRate = Integer.parseInt(br.readLine());
			String TCP_or_UDP = br.readLine();
			int insertion_rate = Integer.parseInt(br.readLine());
			int target_bench = Integer.parseInt(br.readLine()); 
			br.close();
			
			//run the server
			textServer(port, dataDirectory, bufferSize, linesRate,maxRate, TCP_or_UDP, insertion_rate, target_bench);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void textServer(int port, String dataDirectory,
			int bufferSize, int linesRate, int maxRate, String TCP_or_UDP, int insertion_rate, int target_bench) {
		TextServer server = new TextServer(port, dataDirectory, bufferSize, linesRate, maxRate, TCP_or_UDP, insertion_rate, target_bench);
		server.start();
	}

	private int port;
	private String dataDirectory;
	private int bufferSize;
	private int linesRate;
	private int maxRate;
	private String TCP_or_UDP;
	private int insertion_rate;
	private int target_bench;
	private ArrayBlockingQueue<String> queue;
	
	
	public TextServer(int port, String dataDirectory, int bufferSize,
			int linesRate, int maxRate, String TCP_or_UDP, int insertion_rate, int target_bench) {
		this.port = port;
		this.dataDirectory = dataDirectory;
		this.bufferSize = bufferSize;
		this.linesRate = linesRate;
		this.maxRate = maxRate;
		this.TCP_or_UDP = TCP_or_UDP;
		this.insertion_rate = insertion_rate;
		this.target_bench = target_bench;
		this.queue = new ArrayBlockingQueue<String>(bufferSize);		
	}
	

	private void start() {
		Thread producer_thread  = TextServer.startReaderThread(queue, dataDirectory, bufferSize, insertion_rate);
		stall(1);//fill data for 1 seconds
		Thread consumer_thread  = TextServer.startServerRequestsThread(queue, port, linesRate, maxRate, TCP_or_UDP, target_bench);
		
		try {
			consumer_thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//server thread is dead now, terminate the server
		producer_thread.interrupt();
		queue.clear();
	}

	private static Thread startServerRequestsThread(
			ArrayBlockingQueue<String> queue2, int port2, int linesRate2, int maxRate,
			String TCP_or_UDP2, int target_bench) {
		
		Thread t = new Thread(new TextConsumer(queue2, port2, linesRate2, maxRate, TCP_or_UDP2, target_bench));
		t.start();
		return t;
	}

	private static Thread startReaderThread(ArrayBlockingQueue<String> queue2,
			String dataDirectory2, int bufferSize2, int insertion_rate) {
		
		Thread t = new Thread(new TextProducer(queue2, dataDirectory2, bufferSize2, insertion_rate));
		t.start();
		return t;
	}

	private void stall(int seconds) {
		long stallSize = seconds * linesRate;
		int cycles = 0;
		while(queue.size() < stallSize)
		{
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			++cycles;
			System.out.println(cycles+") Stalling...");
		}
	}
}
