import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Paths;
import java.util.Properties;

import edu.umn.cs.kite.common.KiteInstance;
import edu.umn.cs.kite.common.KiteLaunchTool;
import edu.umn.cs.kite.querying.MQL;
import edu.umn.cs.kite.querying.MQLResults;
import edu.umn.cs.kite.querying.metadata.MetadataEntry;
import javafx.util.Pair;

public class BenchHandler implements Runnable {
	private ServerSocket listener;
	private boolean is_kite_initialized = false;
	
	public BenchHandler(ServerSocket listener) {
		this.listener = listener;
	}
	
	@Override
	public void run() {
		Socket socket = null;
		BufferedReader buffer = null;
		
		try {
			// block until response
			socket = listener.accept();
			
			buffer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter buffer_out = new PrintWriter(socket.getOutputStream(), true);
						
			while (!is_kite_initialized) {
		 		Thread.sleep(10);
			}
			
			buffer_out.println("initialized");
			
			String request = new String();
			
			int accum = 0;
			 while ((request = buffer.readLine()) != null) {				
				 if (request.toLowerCase().compareTo("finish") == 0) {
					 break;					 	 
				 } else {
					 System.out.println("Received: " + request);				
					 accum += Integer.parseInt(request);
				 }
			}
			 
			System.out.println("[log] Running benchmark [" + accum + "]...");
			
			run_bench();		
			
			buffer_out.println("close");
			buffer.close();
			socket.close();
			
			System.out.println("[log] Running benchmark [" + accum + "]... Done!");
			
		} catch (IOException | InterruptedException e) { 
			e.printStackTrace();
		}
	}
	
	private void run_bench() {
		String settings = Paths.get(".").toAbsolutePath().normalize().toString() + "/kite.settings";

		String maxQueries = "10";
		String queriesData = "/check/checkins_usa.dat";
				
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(settings));
			
			maxQueries = prop.getProperty("maxQueries");
			queriesData = prop.getProperty("queriesData");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		final int max_queries = Integer.parseInt(maxQueries);
		final float default_radius = 30;
		final String file = Paths.get(".").toAbsolutePath().normalize().toString() + queriesData;
		
		final float PI_180_INV = 180.f / (float)(3.14159265358979323846);
		final float PI_180 = (float)(3.14159265358979323846) / 180.f;
		final float r_earth = 6378.f;
		
		String statement = null;
		Pair<Pair<Boolean, String>, MetadataEntry> parsingResults = null;
		Pair<Pair<Boolean, String>, MQLResults> mqlResults = null;
		
		long startTime, endTime;
		
		try {
			DataInputStream data = new DataInputStream(new FileInputStream(file));
			
			// skip header
			data.skip(4);
			
			for (int id = 0; data.available() > 0 && id < max_queries; ++id) {
				// read coordinates into buffer
				byte[] el = new byte[8];
				data.readFully(el);
				
				float lat = ByteBuffer.wrap(el, 0, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();
				float lon = ByteBuffer.wrap(el, 4, 4).order(ByteOrder.LITTLE_ENDIAN).getFloat();

				// bounding box
				float lat0 = lat + (default_radius / r_earth) * (PI_180_INV);
				
				if (lat0 < -85.051132f) lat0 = -85.051132f;
				else if (lat0 > 85.051132f) lat0 = 85.051132f;
				
				float lon0 = (float) (lon - (default_radius / r_earth) * (PI_180_INV) / Math.cos(lat * PI_180));
				
				if (lon0 < -180.f) lon0 = -180.f;
				else if (lon0 > 180.f) lon0 = 180.f;
				
				float lat1 = lat - (default_radius / r_earth) * (PI_180_INV);
				
				if (lat1 < -85.051132f) lat1 = -85.051132f;
				else if (lat1 > 85.051132f) lat1 = 85.051132f;
				
				float lon1 = (float) (lon + (default_radius / r_earth) * (PI_180_INV) / Math.cos(lat * PI_180));
				
				if (lon1 < -180.f) lon1 = -180.f;
				else if (lon1 > 180.f) lon1 = 180.f;
				
				StringBuilder csv = new StringBuilder();
				
				if (lat0 == lat1 || lon0 == lon1) {
					System.out.println("[error] invalid spatial range: " + Float.toString(lat) + "," + Float.toString(lon));
					
					csv.append("[false]topk_search_k");
				    csv.append(",");
				    csv.append(id);
				    csv.append(",");
				    csv.append(Integer.toString(-1));
				    csv.append(",");
				    csv.append("ms");
				} else {
					String bound = "["  
							+ Float.toString(lat0) + "," + Float.toString(lat1) + "," 
							+ Float.toString(lon1) + "," + Float.toString(lon0)
							+ "]";
					
					statement = "SELECT * FROM stream1 where location within " + bound + " topk 100 ";
						//+ "TIME [1 Jan 1970 00:00:00 GMT, " + new Date().toGMTString() + "]";
				
					parsingResults = MQL.parseStatement(statement);
					
					// save current System.out
					PrintStream old = System.out;
					
					//redirect output
					System.setOut(new PrintStream(new OutputStream() {
			            public void write(int b) {}
			        }));
					
					startTime = System.nanoTime();
					mqlResults = MQL.executeStatement(parsingResults);
					endTime = System.nanoTime();
					
					// revert output to default
					System.out.flush();
				    System.setOut(old);
				    
				    csv.append("[true]topk_search_k");
				    csv.append(",");
				    csv.append(id);
				    csv.append(",");
				    //csv.append(bound);
				    //csv.append(",");
				    csv.append(mqlResults.getValue().getAnswerSize());
				    csv.append(",");				    
				    csv.append(Long.toString((endTime - startTime) / 1000000));
				    csv.append(",");
				    csv.append("ms");
				}
				
				System.out.println(csv.toString());
			}
			
			data.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void kite_initialized() {
		is_kite_initialized = true;		
	}
}
