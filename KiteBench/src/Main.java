import javafx.util.Pair;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.nio.file.Paths;
import java.util.Properties;

import edu.umn.cs.kite.common.KiteInstance;
import edu.umn.cs.kite.common.KiteLaunchTool;

import edu.umn.cs.kite.querying.MQL;
import edu.umn.cs.kite.querying.metadata.MetadataEntry;

public class Main {

	public static void main(String[] args) {			
		ServerSocket listener = null;
		Thread bench_thread = null;
		BenchHandler bench_handler = null;
		
		try {
			listener = new ServerSocket(9090);
			bench_handler = new BenchHandler(listener);
			
			bench_thread = new Thread(bench_handler);
			bench_thread.start();
			
			while (!bench_thread.isAlive()) {
				Thread.sleep(1);
			}			
		} catch (IOException | InterruptedException e) { 
			e.printStackTrace();
			System.exit(1);
		}
		
		init_kite();
		bench_handler.kite_initialized();
		
		try {
			bench_thread.join();
			listener.close();	
		} catch (IOException | InterruptedException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		close_kite();				
	}
	
	public static void init_kite() {
		String settings = Paths.get(".").toAbsolutePath().normalize().toString() + "/kite.settings";

		// Launch Kite Machine
		KiteLaunchTool kite = new KiteLaunchTool();
		KiteInstance.initSettings(kite, settings);
		
		String streamMQL = "CREATE STREAM stream1 (id:Long, mtime:Timestamp,keyword:String, location:GeoLocation,username:String) "
				+ "FROM Network_TCP(127.0.0.1:1455) "
				+ "FORMAT CSV(0,1,4,3,2)";
		String indexMQL = "CREATE INDEX SPATIAL GRID index1 ON stream1(location) "
				+ "OPTIONS 21600000,50,90,-90,180,-180,1800,3600";
				
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(settings));
			
			streamMQL = prop.getProperty("streamMQL");
			indexMQL = prop.getProperty("indexMQL");
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Pair<Pair<Boolean, String>, MetadataEntry> parsingResults = null;
		
		System.out.println("[log] " + streamMQL);
		parsingResults = MQL.parseStatement(streamMQL);
		MQL.executeStatement(parsingResults);

		// index_capacity: is the maximum number of Microblogs that the index holds in the main memory. The default value is 1000000.
		// num_index_segments: is the number of in-memory index segments. The default value is 5.
		// north, south, east, west: are the bounding box coordinates of the spatial grid index. The default value is the whole world bounding box.
		// num_rows, num_cols: are the number of rows and columns of the spatial grid index. The default values are 180 and 360, respectively.
		
		System.out.println("[log] " + indexMQL);
		parsingResults = MQL.parseStatement(indexMQL);
		MQL.executeStatement(parsingResults);
		
		try {
			Thread.sleep(1);		
		} catch (InterruptedException e) {
			System.exit(1);
		}
	}
	
	public static void close_kite() {
		String statement = null;
		Pair<Pair<Boolean, String>, MetadataEntry> parsingResults = null;
		
		statement = "PAUSE stream1";
		
		System.out.println("[log] " + statement);
		parsingResults = MQL.parseStatement(statement);
		MQL.executeStatement(parsingResults);
		
		statement = "DROP STREAM stream1";
		
		System.out.println("[log] " + statement);
		parsingResults = MQL.parseStatement(statement);
		MQL.executeStatement(parsingResults);
		
		KiteInstance.closeGracefully(0);
	}
}
